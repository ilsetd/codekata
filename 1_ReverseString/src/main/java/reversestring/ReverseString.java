package reversestring;

public class ReverseString {

	/**
	 * Reverse the given string
	 * bvb: abcdefg becomes: gfedcba
	 * @param string input string
	 * @return the reversed string string
	 */
	public String reverse(String string) {
		throw new UnsupportedOperationException("implement me");
	}

}
