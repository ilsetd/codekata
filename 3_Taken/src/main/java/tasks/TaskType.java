package tasks;

//See readme.txt
public enum TaskType {

	MOW_LAWN,
	CUT_HEDGE,
	EMPTY_BIN,
	FEED_DOG,
	FEED_CAT

}
