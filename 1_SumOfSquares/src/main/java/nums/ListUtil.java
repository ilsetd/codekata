package nums;

import java.util.List;

public class ListUtil {

	/**
	 * Given a list, returns the sum of the squares of each element of the list
	 * @param numbers the list of numbers to calculate the sum of squares for
	 * @return the sum of squares of each of the elements of the list
	 */
	public static Integer sumOfSquares(List<Integer> numbers) {
		throw new UnsupportedOperationException("implement me!");
	}

}
