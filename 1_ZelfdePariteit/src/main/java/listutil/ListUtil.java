package listutil;

import java.util.List;

public class ListUtil {

	/**
	 * Returns, given an integer and a list of integers
	 * a list of all integers with the same parity
	 * Example: 1, (1 2 3 4 5) => (1 3 5)
	 * @param integer integer to compare with
	 * @param aList the list of integers
	 * @return a list of integers with the same parity as integer
	 */
	public static List<Integer> sameParity(Integer integer, List<Integer> aList) {
		throw new UnsupportedOperationException("implement me!");
	}

}
