package factorial;

public class Factorial {

	/**
	 * Factorial (mathematical operator '!') is the product of all integers from 1 to n.
	 * By definition is 0! = 1.
	 * example: 4! = 4 * 3 * 2 * 1 = 24
	 * @param n n-th factorial
	 * @return n!
	 */
	public static long factorial(long n) {
		throw new UnsupportedOperationException("to be implemented");
	}

}
