package employedworker;

/**
 * An EmployedStudent is both an Employee and a Student
 * An EmployedStudent has the behavior of Employee concerning the year salary and the unique number of Student.
 *
 * Implement EmployedStudent and avoid code duplication where possible
 */
public class EmployedStudent {

	public EmployedStudent(String name, int monthSalary) {
		throw new UnsupportedOperationException("implement me");
	}
		
	public int getYearSalary() {
		throw new UnsupportedOperationException("implement me");
	}
	
	public long getUNumber() {
		throw new UnsupportedOperationException("implement me");
	}
}
