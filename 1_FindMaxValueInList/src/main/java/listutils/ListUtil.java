package listutils;


public class ListUtil {

	/**
	 * Given a list of integers, this function shall return the highest integer.
	 * @param aList a list of integer values
	 * @return the maximum value
	 * @throws IllegalArgumentException when the list is empty
	 */
	public static Integer findMaxValueInList(Integer... aList) {
		throw new UnsupportedOperationException("implement me");
	}

}
