package decoding;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class LengthDecodingAlgorithmTest {

	private LengthDecodingAlgorithm lengteDecodering;

	@Before
	public void setup() throws Exception {
		lengteDecodering = new LengthDecodingAlgorithm();
	}

	@Test
	public void legeString() {
		assertEquals("", lengteDecodering.decode(""));
	}

	@Test
	public void geenDecoderingNodig() {
		assertEquals("abc", lengteDecodering.decode("abc"));
	}

	@Test
	public void decodeerVolledigeString() {
		assertEquals("aaa", lengteDecodering.decode("3a"));
	}

	@Test
	public void decodeerEersteLetters() {
		assertEquals("aaabc", lengteDecodering.decode("3abc"));
	}

	@Test
	public void decodeerLettersInMidden() {
		assertEquals("abbbbc", lengteDecodering.decode("a4bc"));
	}

	@Test
	public void decodeerLettersOpEinde() {
		assertEquals("abcc", lengteDecodering.decode("ab2c"));
	}

	@Test
	public void meerdereAaneenschakelingenVanHetzelfdeKarakter() {
		assertEquals("aaaabaaaaaaaaaaaaaacaaaaa", lengteDecodering.decode("4ab14ac5a"));
	}

}
