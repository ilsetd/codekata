package decoding;

public class LengthDecodingAlgorithm {

	/**
	 * LengthDecoding is a decompression algorithm. It replaces combinations of a number n and character x
	 * by n times the character x
	 * Example:
	 * 3a2bx2c->aaabbxcc
	 */
	public String decode(String string) {
		throw new UnsupportedOperationException("to be implemented");
	}

}
