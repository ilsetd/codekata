package number_to_words;

public class NumberToWords {

	/**
	 * This method returns, given a number, a string with the number written fully out in words.
	 * Example: numberToWords(22) => twenty two
	 *      numberToWords(342) => three hundred forty two
	 *      numberToWords(335603) => three hundred thirty five thousand six hundred three
	 * @param number an integer number
	 * @return number written fully out in words
	 */
	public String numberToWords(int number) {
		throw new UnsupportedOperationException("Implement me!");
	}

}
