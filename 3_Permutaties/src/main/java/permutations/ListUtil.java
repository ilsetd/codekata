package permutations;

import java.util.List;
import java.util.Set;

public class ListUtil {

	/**
	 * Returns a set with all permutations of a given set
	 * Permutation is every possible ordered sequence.
	 * Example (1 2 3) => ((1 2 3) (1 3 2) (2 1 3) (2 3 1) (3 1 2) (3 2 1))
	 *  	
	 * @param toPermutate a set of integers to apply permutation on
	 * @return a set of permutations of the toPermute set
	 */
	public static Set<List<Integer>> permutate(Set<Integer> toPermutate) {
		throw new UnsupportedOperationException("implement me");
	}

}
