package util;

public class StringUtil {

	/**
	 * This method adds a character c in a string aString with a given interval
	 * Example "blblbl", c="a", interval = 2 becomes "blablabla".
	 *
	 * @param aString the string to interleave
	 * @param c the character add to aString
	 * @param interval repeatedly add character c after _interval_ characters
	 * @return aString with character c added after every interval characters
	 */
	public static String interleave(String aString, char c, int interval) {
		throw new UnsupportedOperationException("implement me");
	}

}
