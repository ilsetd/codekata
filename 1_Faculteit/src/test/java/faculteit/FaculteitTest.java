package faculteit;

import org.junit.Test;

import static faculteit.Faculteit.faculteit;
import static org.assertj.core.api.Assertions.assertThat;

public class FaculteitTest {
	
	@Test
	public void nultemtwintigFaculteit() {
		assertThat(faculteit(0)).isEqualTo(1);
		assertThat(faculteit(1)).isEqualTo(1);
		assertThat(faculteit(2)).isEqualTo(2);
		assertThat(faculteit(3)).isEqualTo(6);
		assertThat(faculteit(4)).isEqualTo(24);
		assertThat(faculteit(5)).isEqualTo(120);
		assertThat(faculteit(6)).isEqualTo(720);
		assertThat(faculteit(7)).isEqualTo(5040);
		assertThat(faculteit(8)).isEqualTo(40320);
		assertThat(faculteit(9)).isEqualTo(362880);
		assertThat(faculteit(10)).isEqualTo(3628800);
		assertThat(faculteit(11)).isEqualTo(39916800);
		assertThat(faculteit(12)).isEqualTo(479001600);
		assertThat(faculteit(13)).isEqualTo(6227020800l);
		assertThat(faculteit(14)).isEqualTo(87178291200l);
		assertThat(faculteit(15)).isEqualTo(1307674368000l);
		assertThat(faculteit(16)).isEqualTo(20922789888000l);
		assertThat(faculteit(17)).isEqualTo(355687428096000l);
		assertThat(faculteit(18)).isEqualTo(6402373705728000l);
		assertThat(faculteit(19)).isEqualTo(121645100408832000l);
		assertThat(faculteit(20)).isEqualTo(2432902008176640000l);
	}
	

}
