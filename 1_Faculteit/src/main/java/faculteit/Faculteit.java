package faculteit;

public class Faculteit {

	/**
	 * Faculteit (wiskundige operator '!') is het product van de getallen van 1 tem n. 
	 * Per definitie geldt dat 0! = 1.
	 * bvb: 4! = 4 * 3 * 2 * 1 = 24
	 * @param n nde faculteit
	 * @return de faculteit tem n
	 */
	public static long faculteit(long n) {
		throw new UnsupportedOperationException("to be implemented");
	}

}
