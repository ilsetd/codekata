package num;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

public class SchrijfInRomeinseCijfersTest {
	
	@Test
	public void individueleRomeinseGetallen() {
		assertEquals("I", num.GetalUtil.alsRomeinsCijfer(1));
		assertEquals("V", num.GetalUtil.alsRomeinsCijfer(5));
		assertEquals("X", num.GetalUtil.alsRomeinsCijfer(10));
		assertEquals("L", num.GetalUtil.alsRomeinsCijfer(50));
		assertEquals("C", num.GetalUtil.alsRomeinsCijfer(100));
		assertEquals("D", num.GetalUtil.alsRomeinsCijfer(500));
		assertEquals("M", num.GetalUtil.alsRomeinsCijfer(1000));
	}
	
	@Test
	public void optellendeRomeinseCijfers() {
		assertEquals("II", num.GetalUtil.alsRomeinsCijfer(2));
		assertEquals("III", num.GetalUtil.alsRomeinsCijfer(3));
		assertEquals("VI", num.GetalUtil.alsRomeinsCijfer(6));
		assertEquals("VII", num.GetalUtil.alsRomeinsCijfer(7));
		assertEquals("VIII", num.GetalUtil.alsRomeinsCijfer(8));
		assertEquals("XI", num.GetalUtil.alsRomeinsCijfer(11));
		assertEquals("XVII", num.GetalUtil.alsRomeinsCijfer(17));
		assertEquals("XXXVIII", num.GetalUtil.alsRomeinsCijfer(38));
		assertEquals("LXXII", num.GetalUtil.alsRomeinsCijfer(72));
		assertEquals("LXXX", num.GetalUtil.alsRomeinsCijfer(80));
		assertEquals("CXXII", num.GetalUtil.alsRomeinsCijfer(122));
		assertEquals("CLIII", num.GetalUtil.alsRomeinsCijfer(153));
		assertEquals("DCLXXVIII", num.GetalUtil.alsRomeinsCijfer(678));
		assertEquals("MCCXXXIII", num.GetalUtil.alsRomeinsCijfer(1233));
		assertEquals("MMMCCLXXII", num.GetalUtil.alsRomeinsCijfer(3272));
	}
	
	@Test
	public void aftrekkendeRomeinseCijfers() {
		assertEquals("IV", num.GetalUtil.alsRomeinsCijfer(4));
		assertEquals("IX", num.GetalUtil.alsRomeinsCijfer(9));
		assertEquals("XXXIX", num.GetalUtil.alsRomeinsCijfer(39));
		assertEquals("XLII", num.GetalUtil.alsRomeinsCijfer(42));
		assertEquals("XCIX", num.GetalUtil.alsRomeinsCijfer(99));
		assertEquals("CIX", num.GetalUtil.alsRomeinsCijfer(109));
		assertEquals("CXIX", num.GetalUtil.alsRomeinsCijfer(119));
		assertEquals("CXXIX", num.GetalUtil.alsRomeinsCijfer(129));
		assertEquals("CXXXIX", num.GetalUtil.alsRomeinsCijfer(139));
		assertEquals("CLXIX", num.GetalUtil.alsRomeinsCijfer(169));
		assertEquals("CDIII", num.GetalUtil.alsRomeinsCijfer(403));
		assertEquals("CMLXXIX", num.GetalUtil.alsRomeinsCijfer(979));
		assertEquals("MCMXCIX", num.GetalUtil.alsRomeinsCijfer(1999));
	}
}
