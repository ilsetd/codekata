package getalvoluit;

public class GetalVoluit {

	/**
	 * Deze methode roept voluit de getallen tem een miljoen
	 * bvb: roepvoluit(22) => ��n en twintig
	 *      roepvoluit(342) => drie honderd en twee en veertig
	 *      roepvoluit(335603) => drie honderd en vijf en dertig duizend en zes honderd en drie 
	 * @param getal getal dat voluit moet geroepen worden
	 * @return de string die moet geroepen worden
	 */
	public String roepvoluit(int getal) {
		throw new UnsupportedOperationException("Implement me!");
	}

}
