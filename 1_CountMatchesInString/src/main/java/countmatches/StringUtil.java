package countmatches;

public class StringUtil {

	/**
	 * Method counts the occurences of a character  in a string
	 * @param aString the string to search in for matches of aCharacter
	 * @param aCharacter the character to search with
	 * @return number of occurences of aCharacter in aStrings
	 *
	 */
	public static int countMatches(String aString, char aCharacter) {
		throw new UnsupportedOperationException("implement me");
	}

}
