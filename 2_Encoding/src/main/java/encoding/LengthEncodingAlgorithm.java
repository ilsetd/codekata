package encoding;

public class LengthEncodingAlgorithm {
	
	/**
	 * LengthEncoding is a compression algorithm. It searches for repetitive characters in a string and replaces
	 * them with the number of occurences and the character.
	 * Example:
	 * aaabbxcc->3a2bx2c
	 *
	 */
	public String encode(String string) {
		throw new UnsupportedOperationException("to be implemented");
	}

}
