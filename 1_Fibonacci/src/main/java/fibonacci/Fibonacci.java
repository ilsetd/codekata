package fibonacci;

public class Fibonacci {

	/**
	 * This method calculates the n-th fibanonacci number in the fibonacci sequence. The fibonacci numbers for n=0 is 0
	 * and for n=1 is 1.
	 * All following fibonacci numbers are the sum of the previous two.
	 * Example: 3-th fibonacci number: 2-nd fibonacci number (1)+ 1-th fibonacci number (1) = 2
	 * @param n refers to the n-th element in the fibonacci sequence.
	 * @return the fibonacci number corresponding to the n-th element in the fibonacci sequence
	 */
	public int calculate(int n) {
		throw new UnsupportedOperationException("implement me");
	}

}
