package supersets;

import java.util.Set;

public class SetUtil {

	/**
	 * SuperSets returns a set of all possible combinations of elements from the given set. The order of the result is
	 * not tasks into account.
	 * Example (1 2 3) => (() (1) (2) (3) (1 2) (1 3) (2 3) (1 2 3))
	 * 
	 * @param set the set to calculate the superset of
	 * @return a set containing a set for each combination
	 */
	public static Set<Set<Integer>> superSets(Set<Integer> set) {
		throw new UnsupportedOperationException("Implement me!");
	}

}
