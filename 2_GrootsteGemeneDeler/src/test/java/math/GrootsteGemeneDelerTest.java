package math;


import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GrootsteGemeneDelerTest {
	
	@Test
	public void zelfdeGetallen() {
		Assertions.assertThat(MathUtil.ggd(4, 4)).isEqualTo(4);
	}
	
	@Test
	public void verschillendeGetallen() {
		assertEquals(6, math.MathUtil.ggd(12, 6));
		assertEquals(5, math.MathUtil.ggd(20, 15));
		assertEquals(2, math.MathUtil.ggd(206, 40));
		assertEquals(21, math.MathUtil.ggd(252, 105));
		assertEquals(60, math.MathUtil.ggd(1140, 900));
		assertEquals(4, math.MathUtil.ggd(752, 372));
	}

}
