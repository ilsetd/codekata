package teaching;


public class Teacher {

	/**
	 * This method translates a percentage to a grade.
	 * ex. 52 -> E, 84 -> B
	 * 
	 * @param percentage the achieved percentage between 0 and 100
	 * @return the achieved grade
	 * @throws IllegalArgumentException if the percentage < 0 or > 100
	 */
	public static char gradeMe(int percentage) {
		throw new UnsupportedOperationException("implement me");
	}
	
}
