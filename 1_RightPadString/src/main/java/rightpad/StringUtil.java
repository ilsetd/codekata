package rightpad;

public class StringUtil {

	/**
	 * Right pad a string with a specified character
	 * @param aString the string to pad out, maybe null
	 * @param size the size to pad to
	 * @param padChar the char to pad with
	 * @return the padded string
	 */
	public String rightPad(String aString, int size, char padChar) {
		throw new UnsupportedOperationException("Implement me");
	}

}
